package main

import (
	"auth-webhook/server"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
)

var log = logrus.New()

var rootCmd = &cobra.Command{
	Use:   "auth-webhook-server",
	Short: "`Auth webhook server`",
	Long: `Auth webhook server`,
}

const (
	allowed_log_levels = "ERROR, WARN, INFO, DEBUG, TRACE"
)

func main() {
	const (
		default_log_level = "INFO"
		default_hostname = "localhost"
		default_port = 443
		default_cert_file = "/tls/server.crt"
		default_key_file = "/tls/server.key"
		default_config_file = "/cfg/accounts.cfg"
	)

	var log_level string
	var hostname string
	var port int
	var cert_file string
	var key_file string
	var config_file string

	//Initialise logging
	log.Out = os.Stdout
	log.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})
	setLogLevel(default_log_level)

	var cmdStart = &cobra.Command{
		Use:   "start",
		Short: "Start server",
		Long: `Start server`,
		Run: func(cmd *cobra.Command, args []string) {
			setLogLevel(log_level)
			server.New(log, hostname, port, cert_file, key_file, config_file).StartAndBlock()
		},
	}
	cmdStart.PersistentFlags().StringVar(&log_level, "loglevel", default_log_level, fmt.Sprintf("Set verbosity to the specified level. Allowed values: %s", allowed_log_levels))
	cmdStart.PersistentFlags().StringVar(&hostname, "hostname", default_hostname, "Server hostname")
	cmdStart.PersistentFlags().IntVar(&port, "port", default_port, "Server port")
	cmdStart.PersistentFlags().StringVar(&cert_file, "cert", default_cert_file, "Certificate file path")
	cmdStart.PersistentFlags().StringVar(&key_file, "key", default_key_file, "Key file path")
	cmdStart.PersistentFlags().StringVar(&config_file, "config", default_config_file, "Path to config file with accounts")

	rootCmd.AddCommand(cmdStart)
	rootCmd.Execute()
}

func setLogLevel(level string) {
	switch(level) {
	case "TRACE": log.SetLevel(logrus.TraceLevel)
	case "DEBUG": log.SetLevel(logrus.DebugLevel)
	case "INFO": log.SetLevel(logrus.InfoLevel)
	case "WARN": log.SetLevel(logrus.WarnLevel)
	case "ERROR": log.SetLevel(logrus.ErrorLevel)
	default:
		log.Fatalf("Unsupported log level: %s. Allowed values: %s.", level, allowed_log_levels)
	}
	log.Tracef("Setting log level: %v", log.Level)
}