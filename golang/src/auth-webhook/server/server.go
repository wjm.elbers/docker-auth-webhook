package server

import (
	"crypto/rand"
	"crypto/tls"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"os"
	"strings"
)

const (
	TOKEN_LENGTH = 16
)

type Server struct {
	hostname string
	port int
	cert_file string
	key_file string
	configFile string
	log *logrus.Logger
	users map[string]User
}

func New(log *logrus.Logger, hostname string, port int, cert_file, key_file, configFile string) *Server {
	return &Server{log: log, hostname: hostname, port: port, cert_file: cert_file, key_file: key_file, users: map[string]User{}, configFile: configFile};
}

func (s *Server) StartAndBlock() {
	if _, err := os.Stat(s.cert_file); os.IsNotExist(err) {
		s.log.Fatalf("Certificate file (%s) not found", s.cert_file)
	}
	if _, err := os.Stat(s.key_file); os.IsNotExist(err) {
		s.log.Fatalf("Private key file (%s) not found", s.key_file)
	}

	//Load user definitions
	var users UsersConfig
	if _, err := toml.DecodeFile(s.configFile, &users); err != nil {
		log.Fatal(err)
	}

	s.log.Infof("Users accounts loaded:")
	//Create indices
	defaultRole := "role"
	for _, u := range users.Users {
		role := defaultRole
		if u.Role != "" {
			role = u.Role
		}
		s.log.Infof("  User: %s, role: %s", u.Username, u.Role)
		s.users[u.Username] = User{id: u.Id, username: u.Username, password: u.Password, role: role, token: nil}
	}

	//Configure CORS
	originsOk := []string{"*"}
	c := cors.New(cors.Options{
		AllowedOrigins: originsOk,
		AllowCredentials: false,
		Debug: false,
	})

	router := mux.NewRouter()
	router.Use(s.requestLoggingMiddleware)
	router.HandleFunc("/validate", s.validate).Methods("GET")
	router.HandleFunc("/fetch", s.fetch).Methods("POST")

	//TLS parameters
	cfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}

	//Start blocking server
	address := fmt.Sprintf("%s:%d", s.hostname, s.port)
	s.log.Printf("Starting server: %s\n", address)
	srv := &http.Server{
		Addr:         address,
		Handler:      c.Handler(router),
		TLSConfig:    cfg,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}
	if err := srv.ListenAndServeTLS(s.cert_file, s.key_file); err != nil {
		s.log.Errorf("Failed to start server. Error: %s", err)
	}
}


func (s *Server) requestLoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.log.Infof("Request: uri=%s, method=%s", r.RequestURI, r.Method)
		next.ServeHTTP(w, r)
	})
}

func (s *Server) fetch(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var t AuthRequest
	err := decoder.Decode(&t)
	if err != nil {
		s.log.Errorf("Error fetching user token. Error: %s\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	s.log.Debugf("Fetch token request for username=%s\n", t.Username)

	if _, ok := s.users[t.Username]; !ok {
		s.log.Infof("User=%s not found\n", t.Username)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	val := s.users[t.Username]
	if s.users[t.Username].password != t.Password {
		s.log.Debugf("User=%s invalid password\n", t.Username)
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(AuthResponse{Token: nil})
	}

	//(re)Generate token if needed
	if val.token == nil {
		new_token := s.generateSecureToken(TOKEN_LENGTH)
		val.token = &new_token
		s.users[t.Username] = val
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(AuthResponse{Token: val.token})
	return
}

func (s *Server) generateSecureToken(length int) string {
	b := make([]byte, length)
	if _, err := rand.Read(b); err != nil {
		return ""
	}
	return hex.EncodeToString(b)
}

func (s *Server) validate(w http.ResponseWriter, r *http.Request) {
	authenticated_user, err := s.authenticate_user(r.Header, false)
	if err != nil {
		s.log.Errorf("Token validation failed. Reason: %s\n", err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	s.log.Infof("Validated token for user: %s", authenticated_user.username)

	t := "nil"
	if authenticated_user.token != nil {
		t = *authenticated_user.token
	}
	s.log.Debugf("Token for user:%s = %s", authenticated_user.username, t)

	w.Header().Add("Content-Type", "application/json")

	body := map[string]interface{}{}
	body["X-Hasura-User-Id"] =  fmt.Sprintf("%s", authenticated_user.id)
	body["X-Hasura-Role"] = authenticated_user.role
	body["X-Hasura-Is-Owner"] = fmt.Sprintf("%t", authenticated_user.owner)
	//body["X-Hasura-Custom"] = ""

	body["Cache-Control"] = "max-age=600" //Relative expiration time in seconds
	//body["Expires"] = "Mon, 30 Mar 2020 13:25:18 GMT" //Absolute expiration, The expected format is "%a, %d %b %Y %T GMT".

	s.log.Debugf("Response Headers:\n")
	for key, value := range w.Header() {
		s.log.Debugf( "> %s=%s\n", key, value)
	}
	s.log.Debugf("Response Body:\n")
	json_data, err := json.Marshal(body)
	if err != nil {
		s.log.Errorf (" Failed to marshal to JSON. Error: %s", err)
	} else {
		s.log.Debugf(" %s", string(json_data))
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(body)
}

/*
[[user]]
id = ""
username = ""
password = ""
role = ""

[[user]]
...
 */
type UsersConfig struct {
	Users []UserConfig `toml:"user"`
}

type UserConfig struct {
	Id string
	Username string
	Password string
	Role string
}

type User struct {
	id string
	username string
	password string
	role string
	owner bool
	token *string
}

type AuthRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type AuthResponse struct {
	Token *string `json:"token"`
}

func (s *Server) authenticate_user(header http.Header, print_headers bool) (*User, error) {
	if print_headers {
		s.log.Debugf("Request Headers:\n")
		for key, value := range header {
			s.log.Debugf("  < %s=%s\n", key, value)
		}
	}

	authZHeaderName := "Authorization"
	if _, ok := header[authZHeaderName]; !ok {
		return nil, fmt.Errorf("Authorization header not found")
	}

	value := header[authZHeaderName]
	if !strings.HasPrefix(value[0], "Bearer") {
		return nil, fmt.Errorf("Authorization value does not start with 'Bearer '")
	}

	fields := strings.Fields(value[0])
	if len(fields) != 2 {
		return nil, fmt.Errorf("Expected 2 fields for bearer value, got %d fields instead", len(fields))
	}

	for _, val := range s.users {
		if val.token != nil && *val.token == fields[1] {
			return &val, nil
		}
	}

	return nil, fmt.Errorf("No user found for token value = [%s]", fields[1])
}