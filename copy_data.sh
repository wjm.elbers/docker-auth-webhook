#!/bin/bash

BINARY="auth-webhook-server"
GO_PATH="golang/src"
PROJECT_PATH="auth-webhook"

#DOCKER_BUILD_IMAGE="alpine-golang-build:c58318b"
DOCKER_BUILD_IMAGE="registry.gitlab.com/wjm.elbers/docker-golang-build:0.0.9"

init_data (){
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then
        #Remote / gitlab ci
        echo "Building ${BINARY} remotely"
        cd ..
        docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${GO_PATH}" -v "$PWD/${GO_PATH}":/go/src/ -w /go/src/${PROJECT_PATH} ${DOCKER_BUILD_IMAGE} make
        mv "./${GO_PATH}/${PROJECT_PATH}/${BINARY}" ./image && \
        cd image
    else
        #Local build
        cd ..
        echo "Building ${BINARY} locally, path=$PWD"
	      docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${GO_PATH}" -v "$PWD/${GO_PATH}":/go/src/ -w /go/src/${PROJECT_PATH} ${DOCKER_BUILD_IMAGE} make
        mv "./${GO_PATH}/${PROJECT_PATH}/${BINARY}" ./image
        cd ./image
    fi
}

cleanup_data () {
    echo "Removing ${BINARY}"
    rm -f "${BINARY}"
}
